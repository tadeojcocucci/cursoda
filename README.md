# mini curso DA

Pueden bajar el material del minicurso del [acá](https://bitbucket.org/tadeojcocucci/cursoda/downloads/)

* El archivo guia1.pdf contiene los ejercicios
* OI.ipynb contiene código para estudiar el problema 1
* bayes.ipynb contiene código para estudiar el problema 2
* DA.ipynb contiene código para estudiar el problema 3